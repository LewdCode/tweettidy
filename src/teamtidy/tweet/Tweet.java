package teamtidy.tweet;

import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.stream.Stream;

import org.apache.commons.io.FileUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import twitter4j.Status;
import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;

import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.wb.swt.SWTResourceManager;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.ShellAdapter;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;

public class Tweet {
	protected Shell shell;
	private boolean first = true;
	private String ur = "";
	private boolean run = true;
	private static int noSpam = 1;
	private static Text tweet;
	private static Text cKey;
	private static Text cSec;
	private static Text aTok;
	private static Text aSec;
	private Label lblTweetToSend;
	private Label lblDelayinMillis;
	private Text delay;
	private Button btnFaq;
	private Label lblVersion;
	private Button btnSaveKeysTo;
	private Button btnLoadSavedKeys;
	private Button btnSaveAsTemplate;
	private Button btnLoadTemplate;
	private Button btnSendTweetOnce;
	private Label logo;
	private Button btnDeleteSavedKeys;
	private Label lblGameName;
	private Button btnNewButton_1;
	private Text hitbox;
	private Button btnEnd;
	
	public void saveToDisk(){
		try {
			FileUtils.writeStringToFile(new File("now.stream"), "");
			FileUtils.writeStringToFile(new File("co.key"), cKey.getText());
			FileUtils.writeStringToFile(new File("cs.key"), cSec.getText());
			FileUtils.writeStringToFile(new File("at.key"), aTok.getText());
			FileUtils.writeStringToFile(new File("as.key"), aSec.getText());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void loadcoDisk(){
		try (BufferedReader br = new BufferedReader(new FileReader("co.key")))
		{

			String sCurrentLine;

			while ((sCurrentLine = br.readLine()) != null) {
				cKey.setText(sCurrentLine);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
	public void loadcsDisk(){
		try (BufferedReader br = new BufferedReader(new FileReader("cs.key")))
		{

			String sCurrentLine;

			while ((sCurrentLine = br.readLine()) != null) {
				cSec.setText(sCurrentLine);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
	public void loadatDisk(){
		try (BufferedReader br = new BufferedReader(new FileReader("at.key")))
		{

			String sCurrentLine;

			while ((sCurrentLine = br.readLine()) != null) {
				aTok.setText(sCurrentLine);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
	public void loadasDisk(){
		try (BufferedReader br = new BufferedReader(new FileReader("as.key")))
		{

			String sCurrentLine;

			while ((sCurrentLine = br.readLine()) != null) {
				aSec.setText(sCurrentLine);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
	public void loadDisk(){
		loadcoDisk();
		loadcsDisk();
		loadatDisk();
		loadasDisk();
	}
	public void downloadAssets(){
		URL url;
		URLConnection con;
		DataInputStream dis;
		FileOutputStream fos;
		byte[] fileData;
		try{
			url = new URL("http://puu.sh/nI4LI.png");
			con = url.openConnection();
			con.setRequestProperty("User-Agent", "Mozilla/5.0");
			dis = new DataInputStream(con.getInputStream());
			fileData = new byte[con.getContentLength()];
			for (int q = 0; q < fileData.length; q++) {
				fileData[q] = dis.readByte();
			}
			dis.close();
			fos = new FileOutputStream(new File("logo2.png"));
			fos.write(fileData);
			fos.close();
		}
		catch(Exception m) {
			System.out.print(m);
		}
	}

	public static void doTweet() throws TwitterException{
		String cKeyT = cKey.getText();
		String cSecT = cSec.getText();
		String aTokT = aTok.getText();
		String aSecT = aSec.getText();
		String tweetContent = tweet.getText();
		Random ran = new Random();
		int n = ran.nextInt(2000) + noSpam;
		TwitterFactory twitterFac = new TwitterFactory();
		Twitter twit = twitterFac.getInstance();
		twit.setOAuthConsumer(cKeyT, cSecT);
		twit.setOAuthAccessToken(new AccessToken(aTokT, aSecT));
		String tweet = tweetContent + " " + n;
		StatusUpdate status = new StatusUpdate(tweet);
		@SuppressWarnings("unused")
		Status stat = twit.updateStatus(status);
	}
	final Timer timer = new Timer();
	class TweetLoop extends TimerTask{

		@Override
		public void run() {
			Display.getDefault().asyncExec(new Runnable(){
				public void run(){	
					try {
						doTweet();
					} catch (TwitterException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
			//hitbox.append("Hello \n");
			/*
			*/
		}
		
	}
	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			Tweet window = new Tweet();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		File co = new File("co.key");
		File cs = new File("cs.key");
		File at = new File("at.key");
		File as = new File("as.key");
		File check = new File("logo2.png");
		File stream = new File("now.stream");
		

		
		shell = new Shell(SWT.APPLICATION_MODAL | SWT.MIN | SWT.CLOSE);
		shell.addShellListener(new ShellAdapter() {
			@Override
			public void shellClosed(ShellEvent e) {
				if(cKey.getText().length() > 0 && cSec.getText().length() > 0 && aTok.getText().length() > 0 && aSec.getText().length() > 0){
					if(!(co.exists() && cs.exists() && at.exists() && as.exists())){
						if(MessageDialog.openConfirm(shell, "Unsaved Keys", "It would appear that you have not saved \nyour Keys. Would you like to do so now?")){
							saveToDisk();
							timer.cancel();
							timer.purge();
							try{
							Thread.sleep(500);
							shell.close();
							}catch (InterruptedException ie){}
						}
					}
				}
				timer.cancel();
				timer.purge();
				
			}
		});
		shell.setSize(650, 500);
		shell.setText("#TeamTidy Stream Tweet");
		
		
		
		if(!(check.exists())){
			downloadAssets();
		}
		Label lblConsumerKey = new Label(shell, SWT.NONE);
		lblConsumerKey.setBounds(10, 10, 90, 15);
		lblConsumerKey.setText("Consumer Key");
		
		cKey = new Text(shell, SWT.BORDER);
		cKey.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				if(!(cKey.getText().length() > 0) && first){
				if(co.exists() && cs.exists() && at.exists() && as.exists()){
					if(MessageDialog.openConfirm(shell, "Existing Keys Found", "Existing keys have been found on your system. \n\nWould you like to load them now?")){
						loadDisk();
						first = false;
					}else{
						first = false;
					}
				}
			}
			}
		});
		cKey.setBounds(10, 31, 424, 21);
		
		Label lblConsumerSecret = new Label(shell, SWT.NONE);
		lblConsumerSecret.setBounds(10, 58, 90, 15);
		lblConsumerSecret.setText("Consumer Secret");
		
		cSec = new Text(shell, SWT.BORDER);
		cSec.setBounds(10, 79, 424, 21);
		
		Label lblAccessToken = new Label(shell, SWT.NONE);
		lblAccessToken.setBounds(10, 123, 90, 15);
		lblAccessToken.setText("Access Token");
		
		aTok = new Text(shell, SWT.BORDER);
		aTok.setBounds(10, 144, 424, 21);
		
		Label lblAccessTokenSecret = new Label(shell, SWT.NONE);
		lblAccessTokenSecret.setBounds(10, 171, 123, 15);
		lblAccessTokenSecret.setText("Access Token Secret");
		
		aSec = new Text(shell, SWT.BORDER);
		aSec.setBounds(10, 192, 424, 21);
		
		tweet = new Text(shell, SWT.BORDER | SWT.WRAP | SWT.V_SCROLL);
		tweet.setBounds(10, 260, 424, 67);
		
		lblTweetToSend = new Label(shell, SWT.NONE);
		lblTweetToSend.setBounds(10, 239, 112, 15);
		lblTweetToSend.setText("Tweet to Send");
		
		lblDelayinMillis = new Label(shell, SWT.NONE);
		lblDelayinMillis.setBounds(10, 344, 112, 15);
		lblDelayinMillis.setText("Delay (in Minutes)");
		
		delay = new Text(shell, SWT.BORDER);
		delay.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
			char c = e.character;
			if(Character.isDigit(c)){
				
			}else{
				if(e.keyCode == 8 || e.keyCode == 37 || e.keyCode == 39 || e.keyCode == 16777220 || e.keyCode == 16777219){
					e.doit = true;
				}else{
					e.doit = false;
				}
			}
			}
		});
		delay.setBounds(10, 365, 102, 21);
		
		Button btnPreviewinPopup = new Button(shell, SWT.NONE);
		btnPreviewinPopup.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(tweet.getText().length() > 130){
					MessageDialog.openInformation(shell, "Information", "Your desired Tweet Message must be 130 characters or less. "
																		+ "\nThis allows us to add anti-duplication numbers onto each tweet.");
				}else{
					MessageDialog.openInformation(shell, "Tweet Preview", tweet.getText() + " (random number)" );
				}
			}
		});
		btnPreviewinPopup.setBounds(10, 392, 112, 25);
		btnPreviewinPopup.setText("Preview (in Popup)");
		
		Button btnNewButton = new Button(shell, SWT.NONE);
		btnNewButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(tweet.getText().length() > 130){
					MessageDialog.openInformation(shell, "Information", "Your desired Tweet Message must be 130 characters or less. "
																		+ "\nThis allows us to add anti-duplication numbers onto each tweet.");
				}else{
				int d = Integer.parseInt(delay.getText());
				int del = d * 60000;
				if(run){
					timer.scheduleAtFixedRate(new TweetLoop(), 0, del);
				}else{
					timer.cancel();
					timer.purge();
				}
				}
//				boolean running = true;
//				while(running){
//					try {
//						doTweet();
//						Thread.sleep(del);
//					} catch (InterruptedException | TwitterException e1) {
//						// TODO Auto-generated catch block
//						e1.printStackTrace();
//					}
//				}
				
			}
		});
		btnNewButton.setBounds(128, 392, 123, 25);
		btnNewButton.setText("Start Sending!");
		
		btnFaq = new Button(shell, SWT.NONE);
		btnFaq.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				MessageDialog.openInformation(shell, "FAQ", "Q: Are my login details needed?"
															+ "\nA: No. This app uses Twitter API to send tweets"
															+ "\n\nQ: How do I use this app?"
															+ "\nA: Create an app on dev.twitter.com/apps"
															+ "\nand fill out the fields required. If help"
															+ "\nis needed, contact @LewdCode on Twitter"
															+ "\n for a Walkthrough."
															+ "\n\nQ: Can I support the Developer?"
															+ "\nA: Yes. Click the Version Number to be taken to"
															+ "\nmy Donate page"
															+ "\n\nQ: What are the benefits of Donating?"
															+ "\nA: Donating is mostly to motivate me to improve"
															+ "\nthings more, but if you choose to, you can name the"
															+ "\nnext version of the application (except for Streamers"
															+ "\nwho have custom names, ex TIDYXGAMER)"
															+ "\n - NOTE: Inappropriate names will be rejected");
			}
		});
		btnFaq.setBounds(257, 392, 75, 25);
		btnFaq.setText("FAQ");
		
		lblVersion = new Label(shell, SWT.NONE);
		lblVersion.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				try{
					Desktop.getDesktop().browse(new URL("https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=49CYBSJ7EXCQJ").toURI());
				} catch (Exception ex){}		
			}
		});
		lblVersion.setTouchEnabled(true);
		lblVersion.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD | SWT.ITALIC));
		lblVersion.setForeground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		lblVersion.setBounds(0, 456, 284, 15);
		lblVersion.setText("v1.3.8 - BangTidy");
		
		btnSaveKeysTo = new Button(shell, SWT.NONE);
		btnSaveKeysTo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				
				if(cKey.getText().length() > 0 && cSec.getText().length() > 0 && aTok.getText().length() > 0 && aSec.getText().length() > 0){
					saveToDisk();
					MessageDialog.openInformation(shell, "Keys Saved Successfully", "Your Keys have been saved to disk successfully");
				}
				
			}
		});
		btnSaveKeysTo.setBounds(122, 219, 102, 25);
		btnSaveKeysTo.setText("Save Keys to Disk");
		
		btnLoadSavedKeys = new Button(shell, SWT.NONE);
		btnLoadSavedKeys.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(co.exists() && cs.exists() && at.exists() && as.exists()){
				cKey.setText("");
				cSec.setText("");
				aTok.setText("");
				aSec.setText("");
				loadDisk();
				}else{
					MessageDialog.openError(shell, "No Keys Found", "Error: \n\nNo keys for this application were found. \nPlease enter manually or import keys.");
				}
			}
		});
		btnLoadSavedKeys.setBounds(230, 219, 100, 25);
		btnLoadSavedKeys.setText("Load Saved Keys");
		
		btnSaveAsTemplate = new Button(shell, SWT.NONE);
		btnSaveAsTemplate.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try{
					FileUtils.writeStringToFile(new File("template.tweet"), tweet.getText());
				} catch (IOException ex) {
					// TODO Auto-generated catch block
					ex.printStackTrace();
				}
			}
		});
		btnSaveAsTemplate.setBounds(226, 333, 100, 25);
		btnSaveAsTemplate.setText("Save as Template");
		
		btnLoadTemplate = new Button(shell, SWT.NONE);
		btnLoadTemplate.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				String fileName = "template.tweet";
				tweet.setText("");
				try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {

					String line;
					while ((line = br.readLine()) != null) {
						tweet.append(line + "\n");
					}

				} catch (IOException ex) {
					ex.printStackTrace();
				}
			}
		});
		btnLoadTemplate.setBounds(334, 333, 100, 25);
		btnLoadTemplate.setText("Load Template");
		
		btnSendTweetOnce = new Button(shell, SWT.NONE);
		btnSendTweetOnce.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					doTweet();
				} catch (TwitterException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnSendTweetOnce.setBounds(10, 423, 112, 25);
		btnSendTweetOnce.setText("Send Tweet Once");
		Display dis = Display.getDefault();
		Image img = new Image(dis, "logo2.png");
		logo = new Label(shell, SWT.NONE);
		logo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				try{
				Desktop.getDesktop().browse(new URL("http://tidyxgamer.tv").toURI());
			}catch (Exception ex){}
			}
				
		});
		logo.setText(" ");
		logo.setBounds(311, 441, 123, 30);
		logo.setImage(img);
		
		btnDeleteSavedKeys = new Button(shell, SWT.NONE);
		btnDeleteSavedKeys.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(co.exists() && cs.exists() && at.exists() && as.exists()){
					if(MessageDialog.openConfirm(shell, "Confirm", "Are you sure that you wish to Delete your saved Keys. \nThis action cannot be undone!")){
						co.delete();
						cs.delete();
						at.delete();
						as.delete();
						cKey.setText("");
						cSec.setText("");
						aTok.setText("");
						aSec.setText("");
					}
				}
			}
		});
		btnDeleteSavedKeys.setBounds(332, 219, 102, 25);
		btnDeleteSavedKeys.setText("Delete Saved Keys");
		
		Label label = new Label(shell, SWT.SEPARATOR | SWT.VERTICAL);
		label.setBounds(440, 0, 2, 471);
		
		Button btnEnableAdvancedOptions = new Button(shell, SWT.CHECK);
		btnEnableAdvancedOptions.setEnabled(false);
		btnEnableAdvancedOptions.setBounds(453, 9, 181, 16);
		btnEnableAdvancedOptions.setText("Enable Advanced Options");
		
		lblGameName = new Label(shell, SWT.NONE);
		lblGameName.setEnabled(false);
		lblGameName.setBounds(450, 31, 184, 15);
		lblGameName.setText("Game Name:");
		hitbox = new Text(shell, SWT.BORDER | SWT.WRAP | SWT.V_SCROLL);
		hitbox.setEnabled(false);
		hitbox.setEditable(false);
		
		btnNewButton_1 = new Button(shell, SWT.NONE);
		btnNewButton_1.setEnabled(false);
		btnNewButton_1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
			
			}
		});
		btnNewButton_1.setBounds(448, 58, 186, 25);
		btnNewButton_1.setText("New Button");
		
		hitbox.setBounds(448, 104, 76, 140);
		
		btnEnd = new Button(shell, SWT.NONE);
		btnEnd.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				timer.cancel();
				timer.purge();
				
			}
		});
		btnEnd.setBounds(128, 423, 123, 25);
		btnEnd.setText("Stop Sending");
		
		
		
		

	}
}
